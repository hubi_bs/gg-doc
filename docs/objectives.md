---
sidebar_position: 2
---

# Project objectives

## Introduction

This document highlights the project's strategic objectives that were defined for the GeoGirafe project. In addition to this, it presents some much more concrete objectives from a scheduling and organizational point of view that have been defined as part of the replacement of the current GeoMapFish frontend.

## Strategic objectives

The GeoGirafe project has been developed first and foremost to meet the needs of the users who make up the User-Group. Here are the overriding objectives that have been set for the project:

### Governance

It is important that the GeoGirafe project is managed by members of the community, who are the main users of the solution. Important decisions, both strategic and technical, are therefore validated by the users themselves. This approach also reduces the risks associated with dependence on a single external entity.

### Open-Source & Community
   
GeoGirafe, as an open-source and community project, wishes to emphasize the following points:

- **Collaboration**: developers and users must be able to work together, exchange ideas and share knowledge.
  
- **Transparency**: everyone can examine the code, identify problems and suggest improvements, and ensure that the software is reliable and safe. Communication around the project must also be transparent.

- **Community support**: while not all support can be provided by the community, it is important that the first level of support (requests for information, sharing of tips, configuration problems or minor technical issues) can be community-based.

### Sustainability

Sustainability is a crucial project objective. Indeed, geoportal and WebGIS applications represent the core business of users, and provide enormous value. The project's continuity and reliability must be guaranteed, as must its ability to meet present and future needs.

In particular, the following points need to be taken into account:

- **Protected investment**: A project such as this requires significant investment in terms of time, money and human resources. Both strategic and technological choices must be made with the long term in mind.

- **Adaptation to technological evolution**: The IT field is evolving rapidly, with new technologies, emerging standards and changing trends. A sustainable IT project must be able to adapt to these developments and take advantage of the new opportunities offered by technological advances, without needing to call into question the whole architecture of the project.

- **Economic sustainability**: The project must be economically viable over the long term. Development, maintenance and support costs must be kept under control. Migration costs, although unavoidable, must be anticipated and if possible minimized, as they may jeopardize the long-term viability of the project.

### Modularity & extensibility

The modularity of an application offers advantages such as code reusability, simplified maintenance, increased scalability and improved testability. 
It also makes it possible to separate the responsibilities of different project players. Each module can be assigned to a specific team or developer, which promotes better organization of work and clarity of responsibilities. Each team can concentrate on its area of expertise and work autonomously on its assigned modules, increasing efficiency and reducing the risk of code conflicts.

Modularity and separation of responsibilities also facilitate long-term application maintenance. If a specific module needs to be modified or corrected, it's easier to locate, understand and adjust without having to touch other parts of the application. This reduces the risk of regression and enables faster, more targeted maintenance cycles.

As is often the case in open-source projects, modularity plays an essential role in encouraging collaboration and contribution from the community. By adopting a modular approach, open-source projects can enable external contributors to make specific improvements to individual modules without disrupting the overall project.

A modular project also facilitates scalability. Each user will have specific needs, and it's important that custom developments can be added in the easiest possible way, without disrupting the core application.

## SMART Objectives for the integration in GeoMapFish

### Early integration testing

By the end of 2023, the users will be able to easily test the new frontend with their own production data. This will enable more agile development processes, help to reduce the "tunnel-effect" and allow regular feedback from users as early as possible in the project.

### Multiple business partners

By the end of the 2024, we want to secure three strategic business partners with expertise in project development and maintenance to enhance the project's capabilities, improve its efficiency, and provide ongoing support.

### Great documentation

By the end of 2024, the project will have an open, clear, and participative documentation with one unique entry point, encouraging active engagement from stakeholders and the broader community.

### Replace the GeoMapFish actual frontend

GeoGirafe will replace the actual frontend of GeoMapFish until the end-of-life of the GMF 2.7 LTS, in the first part of 2026.

### Approved functionalities

Until 2026, GeoGirafe will integrate all functionalities from the current GeoMapFish frontend. Some of those functionalities are today totally satisfying, and can just be "migrated", but other ones must be redesigned to achieve a higher level of user satisfaction. User feedback surveys will be conducted at different moments to measure the level of user satisfaction and identify areas for further improvement.
