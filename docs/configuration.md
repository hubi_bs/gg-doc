---
sidebar_position: 5
---

# Configuration

This section lists all the configuration entries available in GeoGirafe, and how to configure them.
The whole configuration is done in the `config.json` file.

Every configuration option that has no default value must be set properly to make the application work.

## General

The `general` section contains all the common configuration entries that can be used by any component.

<table>
  <tr>
    <th>Name</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
    <th>Examples</th>
  </tr>
  <tr>
    <td><code>locale</code></td>
    <td><code>String</code></td>
    <td>Locale used when formatting numbers or dates in the application. For example this locale will be used to format the coordinates.</td>
    <td><code>en-US</code></td>
    <td><code>de-CH</code>, <code>fr-FR</code></td>
  </tr>
</table>

## Languages

The `languages` section contains an option for each available language in the application plus other options is an `Array` of `language`.

<table>
  <tr>
    <th>Name</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
    <th>Examples</th>
  </tr>
  <tr>
    <td><code>translations</code></td>
    <td><code>Object</code></td>
    <td>An object containing the list of available translations identified by their shortname. The corresponding value is an array of links to the JSON files containing the translations as value.</td>
    <td>-</td>
    <td>

```json
"translations": {
  "de": ["de.json", "static/de.json"],
  "en": ["https://map.geo.bs.ch/static/X/en.json"],
  "fr": ["fr.json", "static/fr.json", "static/special_translations.json"]
}
```

  </td>
  </tr>
  <tr>
    <td><code>defaultLanguage</code></td>
    <td><code>String</code></td>
    <td>The shortname of the default language that will be used when the application starts.</td>
    <td>-</td>
    <td><code>en</code>, <code>fr</code>, <code>de</code></td>
  </tr>
</table>

## Themes

The `themes` section contains the configuration options linked to the themes.

<table>
  <tr>
    <th>Name</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
    <th>Examples</th>
  </tr>
  <tr>
    <td><code>url</code></td>
    <td><code>String</code></td>
    <td>Link to the GeoMapFish-Compliant <code>themes.json</code></td>
    <td>-</td>
    <td><code>Mock/themes.json</code>, <code>https://map.geo.bs.ch/themes?background=background&interface=desktop</code></td>
  </tr>
  <tr>
    <td><code>defaultTheme</code></td>
    <td><code>String</code></td>
    <td>Name of the default theme, that will be automatically loaded on application start.</td>
    <td><code>''</code></td>
    <td><code>main</code>, <code>cadastre</code></td>
  </tr>
  <tr>
    <td><code>imagesUrlPrefix</code></td>
    <td><code>String</code></td>
    <td>URL Prefix to use when accessing themes images, if there are defined with relative paths in <code>themes.json</code>.</td>
    <td><code>''</code></td>
    <td><code>https://map.geo.ti.ch</code></td>
  </tr>
  <tr>
    <td><code>showErrorsOnStart</code></td>
    <td><code>Boolean</code></td>
    <td>If <code>true</code>, the errors listed in <code>themes.json</code> will be displayed when the application starts. If <code>false</code>, they won't be displayed.</td>
    <td><code>false</code></td>
    <td><code>true</code>, <code>false</code></td>
  </tr>
</table>

## Basemaps

The list of basemaps available in the application is loaded from the <code>themes.json</code> file.
In addition, you can use the `basemaps` section to configure additional options.

<table>
  <tr>
    <th>Name</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
    <th>Examples</th>
  </tr>
  <tr>
    <td><code>show</code></td>
    <td><code>Boolean</code></td>
    <td>Display the basemap control or not. If set to <code>false</code>, the basemap controller won't be visible in the application.</td>
    <td><code>true</code></td>
    <td><code>true</code>, <code>false</code></td>
  </tr>
  <tr>
    <td><code>defaultBasemap</code></td>
    <td><code>String</code></td>
    <td>Name of the default basemap, that will be displayed when the application starts. This is the name of the background map available in <code>themes.json</code>.</td>
    <td><code>''</code></td>
    <td><code>background_color</code>, <code>swissimage</code></td>
  </tr>
  <tr>
    <td><code>OSM</code></td>
    <td><code>Boolean</code></td>
    <td>Enable or disable OpenStreetMap as an additional basemap layer.</td>
    <td><code>false</code></td>
    <td><code>true</code>, <code>false</code></td>
  </tr>
  <tr>
    <td><code>SwissTopoVectorTiles</code></td>
    <td><code>Boolean</code></td>
    <td>Enable or disable the VectorTiles layer from Swisstopo as an additional basemap layer.</td>
    <td><code>false</code></td>
    <td><code>true</code>, <code>false</code></td>
  </tr>
</table>

## Treeview

The `treeview` section contains the configuration options for the layertree.

<table>
  <tr>
    <th>Name</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
    <th>Examples</th>
  </tr>
  <tr>
    <td><code>useCheckboxes</code></td>
    <td><code>Boolean</code></td>
    <td>If <code>true</code>, checkboxes will be used in the treeview to show if a layer has been activated or not. If <code>false</code>, a simple bullet will be used.</td>
    <td><code>false</code></td>
    <td><code>true</code>, <code>false</code></td>
  </tr>
  <tr>
    <td><code>useLegendIcons</code></td>
    <td><code>Boolean</code></td>
    <td> If <code>true</code>, legend icon will be used in the treeview instead of the bullet, if any is defined in the metadata of the layer in the <code>themes.json</code> file. If <code>false</code>, the legend icons won't be used.</td>
    <td><code>false</code></td>
    <td><code>true</code>, <code>false</code></td>
  </tr>
  <tr>
    <td><code>hideLegendWhenLayerIsDeactivated</code></td>
    <td><code>Boolean</code></td>
    <td>If <code>true</code>, the legend of a layer will automatically be hidden if the layer is not activated. If <code>false</code>, the legend will remain visible even if the layer is deactivated.</td>
    <td><code>true</code></td>
    <td><code>true</code>, <code>false</code></td>
  </tr>
  <tr>
    <td><code>defaultIconSize</code></td>
    <td><code>Object</code></td>
    <td>If <code>useLegendIcons</code> has been set to <code>true</code>, the size of the legend icons can be configured here with the <code>width</code> and <code>height</code> properties.</td>
    <td>-</td>
    <td>-</td>
  </tr>
  <tr>
    <td><code>defaultIconSize.width</code></td>
    <td><code>Integer</code></td>
    <td>Width of the icon that will be used for legend icons</td>
    <td><code>20</code></td>
    <td><code>any integer</code></td>
  </tr>
  <tr>
    <td><code>defaultIconSize.height</code></td>
    <td><code>Height</code></td>
    <td>Width of the icon that will be used for legend icons</td>
    <td><code>20</code></td>
    <td><code>any integer</code></td>
  </tr>
</table>

## Search

The `search` section contains the configuration options for the search.

<table>
  <tr>
    <th>Name</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
    <th>Examples</th>
  </tr>
  <tr>
    <td><code>url</code></td>
    <td><code>String</code></td>
    <td>Link to the GeoMapFish-Compliant Search Service. This service has to be compatible with the format of the GeoMapFish Search-Service. The link should contain the following strings:<br/>- <code>###SEARCHTERM###</code>: will be replaced by the text to search.<br/>- <code>###SEARCHLANG###</code>: will be replaced by the search language.</td>
    <td>-</td>
    <td><code>https://map.geo.bs.ch/search?limit=90&partitionlimit=15&interface=desktop&query=###SEARCHTERM###&lang=###SEARCHLANG###</code></td>
  </tr>
  <tr>
    <td><code>objectPreview</code></td>
    <td><code>boolean</code></td>
    <td>Activate or not the object preview on the map when navigating through the search results.</td>
    <td><code>false</code></td>
    <td><code>true</code>, <code>false</code></td>
  </tr>
  <tr>
    <td><code>layerPreview</code></td>
    <td><code>boolean</code></td>
    <td>Activate or not the layer preview on the map when navigating through the search results.</td>
    <td><code>false</code></td>
    <td><code>true</code>, <code>false</code></td>
  </tr>
  <tr>
    <td><code>minResolution</code></td>
    <td><code>number</code></td>
    <td>Configure the minimum resolution to zoom to when searching for an object. Is the object is very small (a point for example), 
    the zoom will stop to this resolution.</td>
    <td><code>0.5</code></td>
    <td><code>any number</code></td>
  </tr>
</table>

## Print

The `print` section contains the configuration options for the print.

<table>
  <tr>
    <th>Name</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
    <th>Examples</th>
  </tr>
  <tr>
    <td><code>url</code></td>
    <td><code>String</code></td>
    <td>Link to the GeoMapFish-Compliant Print Service.</td>
    <td>-</td>
    <td><code>https://map.geo.bs.ch/printproxy/</code></td>
  </tr>
  <tr>
    <td><code>formats</code></td>
    <td><code>String[]</code></td>
    <td>The desired print formats. The provided format must be supported by MapFishPrint. The order is kept in the dropdown.</td>
    <td><code>["png", "pdf"]</code></td>
    <td><code>["png", "pdf", "jpg"]</code></td>
  </tr>
  <tr>
    <td><code>defaultFormat</code></td>
    <td><code>String</code></td>
    <td>The print format selected by default. Only used if listed in <code>formats</code> and supported by MapFishPrint.</td>
    <td>-</td>
    <td><code>"pdf"</code></td>
  </tr>
  <tr>
    <td><code>layouts</code></td>
    <td><code>String[]</code></td>
    <td>A list of allowed layouts name. If configured, the layouts will be filtered using this value.</td>
    <td>-</td>
    <td><code>["1 A4 portrait", "2 A3 landscape"]</code></td>
  </tr>
  <tr>
    <td><code>defaultLayout</code></td>
    <td><code>String</code></td>
    <td>The print layout selected by default. Only used if the name match a layout.</td>
    <td>-</td>
    <td><code>"1 A4 portrait"</code></td>
  </tr>
  <tr>
    <td><code>scales</code></td>
    <td><code>Number</code></td>
    <td>A list of allowed scales for every layouts. If configured, the scales will be filtered using this value.</td>
    <td>-</td>
    <td><code>[50000, 25000, 10000, 2500, 1000]</code></td>
  </tr>
  <tr>
    <td><code>attributeNames</code></td>
    <td><code>String</code></td>
    <td>The print attributes (optional fields) to display as inputs in the panel. The order is kept in the panel.</td>
    <td>-</td>
    <td><code>["legend", "title", "comments"]</code></td>
  </tr>
  <tr>
    <td><code>legend</code></td>
    <td><code>Object</code></td>
    <td>The options for the print legend. See below.</td>
    <td>-</td>
    <td>-</td>
  </tr>
  <tr>
    <td><code>legend.useExtent</code></td>
    <td><code>boolean</code></td>
    <td>Use or not the bbox to get the WMS legend. For QGIS server only.</td>
    <td><code>true</code></td>
    <td><code>false</code></td>
  </tr>
  <tr>
    <td><code>legend.showGroupsTitle</code></td>
    <td><code>boolean</code></td>
    <td>Display or not groups title in the legend. Switching to false is useful to obtains a "flat" legend.</td>
    <td><code>true</code></td>
    <td><code>false</code></td>
  </tr>
  <tr>
    <td><code>legend.label</code></td>
    <td><code>Object&lt;string&#44; boolean></code></td>
    <td>The key is the server type (MapServer, QGIS, etc.), if the value is false the name of the layer will be not displayed. This is used to avoid duplicated title, as text and in the legend image.</td>
    <td>-</td>
    <td><code>&#123;"mapserver": false&#125;</code></td>
  </tr>
  <tr>
    <td><code>legend.params</code></td>
    <td><code>Object&lt;string&#44; &lt;Object&lt;string&#44; unknown&gt;&gt;</code></td>
    <td>The key is the server type (MapServer, QGIS, etc.) or image for an URL from a metadata. The value is some additional parameters set in the query string.</td>
    <td>-</td>
    <td><code>&#123;"mapserver": &#123;"filter": "house"&#125;&#125;</code></td>
  </tr>
</table>

## Share

The `share` section contains the configuration options for the short links.

<table>
  <tr>
    <th>Name</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
    <th>Examples</th>
  </tr>
  <tr>
    <td><code>service</code></td>
    <td><code>String</code></td>
    <td>Type of ShortUrl service we want to use. Supported configuration are:<br/>- <code>gmf</code>: the GeoMapFish backend shortUrl service will be used.<br/>- <code>lstu</code>: the open-source service Let's Shorten That URL will be used.</td>
    <td><code>gmf</code></td>
    <td><code>gmf</code>, <code>lstu</code></td>
  </tr>
  <tr>
    <td><code>createUrl</code></td>
    <td><code>String</code></td>
    <td>The URL that will be used to create the shortUrl.</td>
    <td>-</td>
    <td><code>https://lstu.fr/a</code></td>
  </tr>
</table>

## Bookmarks

The `bookmarks` section contains the configuration options for saving the bookmarks. It the `service` type is `server` the bookmarks will be fetched and posted as JSON.

<table>
  <tr>
    <th>Name</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
    <th>Examples</th>
  </tr>
  <tr>
    <td><code>service</code></td>
    <td><code>String</code></td>
    <td>Type type of service being used</td>
    <td><code>localStorage</code></td>
    <td><code>localStorage</code>, <code>server</code></td>
  </tr>
  <tr>
    <td><code>get</code></td>
    <td><code>String</code></td>
    <td>Optional URL that will be used to get the bookmarks if the service type is <code>server</code></td>
    <td>-</td>
    <td><code>https://yourbackendserver.com/bookmarks</code></td>
  </tr>
  <tr>
    <td><code>post</code></td>
    <td><code>String</code></td>
    <td>Optional URL that will be used to save the bookmarks if the service type is <code>server</code></td>
    <td>-</td>
    <td><code>https://yourbackendserver.com/bookmarks</code></td>
  </tr>
</table>

## Selection

The `selection` section contains the configuration options for the selected/focused features on the map.

<table>
  <tr>
    <th>Name</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
    <th>Examples</th>
  </tr>
  <tr>
    <td><code>defaultFillColor</code></td>
    <td><code>String</code></td>
    <td>Default fill color used when selecting an object in the map.</td>
    <td><code>#ff66667f</code></td>
    <td><code>any color value in #RGBA format</code></td>
  </tr>
  <tr>
    <td><code>defaultStrokeColor</code></td>
    <td><code>String</code></td>
    <td>Default stroke color used when selecting an object in the map.</td>
    <td><code>#ff3333</code></td>
    <td><code>any color value in #RGBA format</code></td>
  </tr>
  <tr>
    <td><code>defaultStrokeWidth</code></td>
    <td><code>Integer</code></td>
    <td>Default stroke width used when selecting an object in the map.</td>
    <td><code>4</code></td>
    <td><code>any integer</code></td>
  </tr>
  <tr>
    <td><code>defaultFocusFillColor</code></td>
    <td><code>String</code></td>
    <td>Default fill color used when focusing an object in the map.</td>
    <td><code>#ff33337f</code></td>
    <td><code>any color value in #RGBA format</code></td>
  </tr>
  <tr>
    <td><code>defaultFocusStrokeColor</code></td>
    <td><code>String</code></td>
    <td>Default stroke color used when focusing an object in the map.</td>
    <td><code>#ff0000</code></td>
    <td><code>any color value in #RGBA format</code></td>
  </tr>
  <tr>
    <td><code>defaultFocusStrokeWidth</code></td>
    <td><code>Integer</code></td>
    <td>Default stroke width used when focusing an object in the map.</td>
    <td><code>4</code></td>
    <td><code>any integer</code></td>
  </tr>
</table>

## Redlining

The `redlining` section contains the configuration options when drawing features on the map.

<table>
  <tr>
    <th>Name</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
    <th>Examples</th>
  </tr>
  <tr>
    <td><code>defaultFillColor</code></td>
    <td><code>String</code></td>
    <td>Default fill color used when drawing features on the map.</td>
    <td><code>#6666ff7f</code></td>
    <td><code>any color value in #RGBA format</code></td>
  </tr>
  <tr>
    <td><code>defaultStrokeColor</code></td>
    <td><code>String</code></td>
    <td>Default stroke color used when drawing features on the map.</td>
    <td><code>#0000ff</code></td>
    <td><code>any color value in #RGBA format</code></td>
  </tr>
  <tr>
    <td><code>defaultStrokeWidth</code></td>
    <td><code>Number</code></td>
    <td>Default stroke width used when drawing features on the map.</td>
    <td><code>2</code></td>
    <td><code>any integer</code></td>
  </tr>
  <tr>
    <td><code>defaultTextSize</code></td>
    <td><code>Number</code></td>
    <td>Default text size used when drawing features on the map.</td>
    <td><code>12</code></td>
    <td><code>any integer</code></td>
  </tr>
  <tr>
    <td><code>defaultFont</code></td>
    <td><code>String</code></td>
    <td>Default font used when drawing features on the map.</td>
    <td><code>Arial</code></td>
    <td><code>Arial</code>, <code>Tahoma</code>, <code>Verdana</code></td>
  </tr>
</table>

## Projections

The `projections` section contains the list of projection that are available in the application.

<table>
  <tr>
    <th>Name</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
    <th>Examples</th>
  </tr>
  <tr>
    <td><code>projections</code></td>
    <td><code>String</code></td>
    <td>An array containing the list of <code>EPSG</code> coordinate systems with their name.</td>
    <td>-</td>
    <td>

```json
"projections": {
  "EPSG:3857": "W-M",
  "EPSG:4326": "WGS84",
  "EPSG:2056": "LV95"
}
```

  </td>
  </tr>
</table>

## Map

The `map` section contains the configuration of the `OpenLayers` map.

<table>
  <tr>
    <th>Name</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
    <th>Examples</th>
  </tr>
  <tr>
    <td><code>srid</code></td>
    <td><code>String</code></td>
    <td>Default SRID of the map.</td>
    <td>-</td>
    <td><code>EPSG:2056</code></td>
  </tr>
  <tr>
    <td><code>scales</code></td>
    <td><code>Array</code></td>
    <td>Array of scales allowed on the map.</td>
    <td>-</td>
    <td><code>[200000, 75000, 40000, 20000, 10000, 7500, 5000, 3500, 2000, 1000, 500, 200, 100, 50]</code></td>
  </tr>
  <tr>
    <td><code>startPosition</code></td>
    <td><code>String</code></td>
    <td>Initial center position when the application starts.</td>
    <td>-</td>
    <td><code>2611000,1267000</code></td>
  </tr>
  <tr>
    <td><code>startZoom</code></td>
    <td><code>Integer</code></td>
    <td>Initial zoom level when the application starts.</td>
    <td>-</td>
    <td><code>any valid integer for the map zoom level</code></td>
  </tr>
  <tr>
    <td><code>maxExtent</code></td>
    <td><code>String</code></td>
    <td>Maximum extent for the map.</td>
    <td>-</td>
    <td><code>2583000,1235000,2650000,1291000</code></td>
  </tr>
  <tr>
    <td><code>constrainScales</code></td>
    <td><code>Boolean</code></td>
    <td>If <code>true</code>, the map will only be allowed to used the specific scales defined in <code>maps.scales</code>.<br/>If <code>false</code>, any scale is allowed.</td>
    <td><code>true</code></td>
    <td><code>true</code>, <code>false</code></td>
  </tr>
  <tr>
    <td><code>showScaleLine</code></td>
    <td><code>Boolean</code></td>
    <td>Show scale line on the map, or not.</td>
    <td><code>true</code></td>
    <td><code>true</code>, <code>false</code></td>
  </tr>
</table>

## Map3D

The `map3d` section contains the configuration of the `Cesium` globe.

<table>
  <tr>
    <th>Name</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
    <th>Examples</th>
  </tr>
  <tr>
    <td><code>terrainUrl</code></td>
    <td><code>String</code></td>
    <td>URL for the 3D map terrain.</td>
    <td>-</td>
    <td><code>https://terrain100.geo.admin.ch/1.0.0/ch.swisstopo.terrain.3d/</code></td>
  </tr>
  <tr>
    <td><code>terrainImagery</code></td>
    <td><code>Object</code></td>
    <td>See below</td>
    <td>-</td>
    <td>-</td>
  </tr>
  <tr>
    <td><code>terrainImagery.url</code></td>
    <td><code>String</code></td>
    <td>The URL of your imagery provider (WMTS in your case), with template parameters as described in the <a href="https://cesium.com/learn/cesiumjs/ref-doc/UrlTemplateImageryProvider.html?classFilter=UrlTemplateImageryProvider#url" target="_bla^nk">Cesium documentation</a>.</td>
    <td>-</td>
    <td><code>https://wmts20.geo.admin.ch/1.0.0/ch.swisstopo.swissimage-product/default/current/4326/&#123;z&#125;/&#123;x&#125;/&#123;y&#125;.jpeg</code></td>
  </tr>
  <tr>
    <td><code>terrainImagery.srid</code></td>
    <td><code>Integer</code></td>
    <td>The SRID of the service used for terrainImagery. Only <code>4326</code> and <code>3857</code> are supported.</td>
    <td>4326</td>
    <td><code>4326</code>, <code>3857</code></td>
  </tr>
  <tr>
    <td><code>terrainImagery.minLoD</code></td>
    <td><code>Integer</code></td>
    <td>The minimal level-of-detail that the imagery provider supports, if not mentioned, when zooming very far, the map may try to load nonexistent tiles that cause network error.</td>
    <td>-</td>
    <td><code>any integer that represents a valid level-of-details</code></td>
  </tr>
  <tr>
    <td><code>terrainImagery.maxLoD</code></td>
    <td><code>Integer</code></td>
    <td>Same as <code>minLoD</code> but for maximal level-of-detail, to prevent loading nonexistent tiles when zooming very close.</td>
    <td>-</td>
    <td><code>any integer that represents a valid level-of-details</code></td>
  </tr>
  <tr>
    <td><code>terrainImagery.coverageArea</code></td>
    <td><code>Array</code></td>
    <td>The rectangle, in degree, that describe the area provided by your WMTS. This parameter prevents requesting tiles that do not exist on the WMTS.</td>
    <td>-</td>
    <td><code>[5.013926957923385, 45.35600133779394, 11.477436312994008, 48.27502358353741]</code></td>
  </tr>
  <tr>
    <td><code>tilesetsUrls</code></td>
    <td><code>Array</code></td>
    <td>Array of URLs for 3D map tilesets.</td>
    <td>-</td>
    <td><code>["https://3d.geo.bs.ch/static/tiles/a57b8783-1d7b-4ca9-b652-d4ead5436ead_2/tileset.json"]</code></td>
  </tr>
</table>
